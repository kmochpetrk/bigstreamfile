package com.example.demo;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import javax.annotation.PostConstruct;



@Component
public class BigFileControllerClient {

    private static final int BATCH_SIZE = 2;


    private WebClient webClient = WebClient.builder().baseUrl("http://localhost:8089").build();;


    @PostConstruct
    public void postConstruct() {
        //webClient = WebClient.builder().baseUrl("http:localhost:8089").build();
        makeCall1();
    }

    private void makeCall() {
        //final Waiter waiter = new Waiter();
        Flux<BigFilePart> fileParts = webClient.get().uri("/pokus").retrieve().bodyToFlux(BigFilePart.class);
        fileParts.map(this::doSomeSlowWork).subscribe(filePart -> {
            //waiter.assertNotNull(person);
            System.out.println("Client subscribes: " + filePart.getSequenceNumber());
            //waiter.resume();
        });
        //waiter.await(3000, 9);
    }

    private void makeCall1() {
        //final Waiter waiter = new Waiter();
        Flux<BigFilePart> fileParts = webClient.get().uri("/pokus").retrieve().bodyToFlux(BigFilePart.class);
        fileParts.subscribe(new MyCustomBackpressureSubscriber<BigFilePart>(BATCH_SIZE));
//        fileParts.map(this::doSomeSlowWork).subscribe(filePart -> {
//            //waiter.assertNotNull(person);
//            System.out.println("Client subscribes: " + filePart.getSequenceNumber());
//            //waiter.resume();
//        });
        //waiter.await(3000, 9);
    }

    private BigFilePart doSomeSlowWork(BigFilePart bigFilePart) {
        try {
            Thread.sleep(4000);
        }
        catch (InterruptedException e) { }
        return bigFilePart;
    }


}
