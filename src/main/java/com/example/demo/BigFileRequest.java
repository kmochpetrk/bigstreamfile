package com.example.demo;

import lombok.Value;

import java.io.Serializable;

@Value
public class BigFileRequest implements Serializable {
    private String fileName;
    private int partSize;
}
