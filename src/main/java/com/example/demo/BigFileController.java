package com.example.demo;

import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@RestController
public class BigFileController {

    private FileController fileController;

    @GetMapping(value = "/pokus", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<BigFilePart> sendNextPartOfFile() {
        if (fileController == null) {
            fileController = new FileController("e:/Images/Splash/16Color/ATHENA.bmp", 20);
        }

        List<BigFilePart> returnList = new ArrayList<>();

        //return Flux.create(sink -> {
            //sink.onRequest(batchSize -> {
                for (int i = 0; i < 10; i++) {
                    LoggerFactory.getLogger(BigFileController.class).info("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                    returnList.add(fileController.createNextBigFilePart());
                }
           // });
        //});
        return Flux.fromIterable(returnList)/*.delayElements(Duration.ofMillis(3000))*/.
                doOnNext(filePart -> System.out.println("Server produces: " + filePart.getSequenceNumber()));
    }

    @GetMapping(value = "/cisla", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<Long> getIntegers() {
        return Flux.interval(Duration.of(1, ChronoUnit.SECONDS));
    }

}
