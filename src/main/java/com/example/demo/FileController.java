package com.example.demo;

import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class FileController {

    private Path path;
    private int partSize;
    private byte[] content;
    private int currentSequenceNumber = 0;

    /**
     * C:/aaa/aaa.txt - win
     * /aaa/aaa.txt - unix
     * @param fullFileName
     */
    public FileController(String fullFileName, int partSize) {
        this(Paths.get(fullFileName), partSize);
    }

    public FileController(Path path, int partSize) {
        this.path = path;
        this.partSize = partSize;
        try {
            this.content = Files.readAllBytes(path);
        } catch (IOException e) {
            LoggerFactory.getLogger(FileController.class).info("Error reading file", e);
            throw new IllegalStateException(e);
        }
    }

    public BigFilePart createNextBigFilePart() {
        Assert.notNull(content, "Error - content null");
        final byte[] bytes = Arrays.copyOfRange(content, currentSequenceNumber * partSize, currentSequenceNumber * partSize + partSize);
        final BigFilePart bigFilePart = new BigFilePart(path.getFileName().toString(), bytes, currentSequenceNumber++);
        return bigFilePart;
    }
}
