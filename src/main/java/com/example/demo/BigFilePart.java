package com.example.demo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BigFilePart implements Serializable {
    private String fileName = null;
    private byte[] content = null;
    private int sequenceNumber = 0;
}
