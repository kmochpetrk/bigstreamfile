package com.example.demo;

import org.reactivestreams.Subscription;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.BaseSubscriber;

class MyCustomBackpressureSubscriber<T extends BigFilePart> extends BaseSubscriber<T> {

    public MyCustomBackpressureSubscriber(int limit) {
        this.limit = limit;
    }

    int consumed;
    int limit = 3;

    @Override
    protected void hookOnSubscribe(Subscription subscription) {
        System.out.println(limit + " elements requested +++++++++++++++++++++++++++++");
        request(limit);
    }

    @Override
    protected void hookOnNext(T value) {
        // do business logic there

        consumed++;
        doSomeSlowWork(value);

        if (consumed == limit) {
            consumed = 0;
            System.out.println("another " + limit + " elements requested +++++++++++++++++++++++++++++");
            request(limit);
        }
    }

    private T doSomeSlowWork(T value) {
        LoggerFactory.getLogger(MyCustomBackpressureSubscriber.class).info("slow work with seq " + value.getSequenceNumber());

        try {
            Thread.sleep(3000);
        }
        catch (InterruptedException e) { }
        return value;
    }
}
