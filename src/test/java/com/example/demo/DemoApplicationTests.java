package com.example.demo;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@AutoConfigureWebTestClient
class DemoApplicationTests {

    @Autowired
    private WebTestClient webTestClient;

    @Test
    void contextLoads() {
    }

    @Test
    public void test1() {
        final FileController fileController = new FileController("e:/Images/Splash/16Color/ATHENA.bmp", 20);

        final Flux<BigFilePart> responseBody = webTestClient.get().uri("/pokus")
                //.contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_STREAM_JSON)
                //.body(Mono.just(new BigFileRequest("e:/Images/Splash/16Color/ATHENA.bmp", 20)),
                //        BigFileRequest.class)
                .exchange().expectStatus().isOk().returnResult(BigFilePart.class).getResponseBody();
        StepVerifier.create(responseBody, 2)
                .expectNext(fileController.createNextBigFilePart())
                //.expectNext(fileController.createNextBigFilePart())
//                .expectNext(fileController.createNextBigFilePart())
                .consumeNextWith(value -> {
                    System.out.println("Next value #2: " + value.getSequenceNumber());
                    try {
                        Thread.sleep(4000L);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                })
                .thenRequest(1)
                .consumeNextWith(value -> {
                    System.out.println("Next value #3: " + value.getSequenceNumber());
                })
                .thenCancel()
                .verify();
    }

    @Test
    public void test2() {
        Flux<Long> longStreamFlux = webTestClient.get().uri("/cisla").accept(MediaType.APPLICATION_STREAM_JSON)
                .exchange()
                .expectStatus().isOk()
                .returnResult(Long.class)
                .getResponseBody();
        StepVerifier.create(longStreamFlux,1)
                .expectNext(Long.valueOf(0))
                .thenRequest(1)
                .expectNext(Long.valueOf(1))
                .thenCancel()
                .verify();
    }

}
